﻿import json
import base64
#init empty id dictionary


baseURL="https://api.tm4j.smartbear.com/rest-api/v2"
#def getTestCases():
#      address=baseURL+"/testcases"
#      request = aqHttp.CreateGetRequest(address)
##      Log.Message(Project.Variables.BearerToken)
##      request.SetHeader("Authorization", Project.Variables.BearerToken)
#      request.SetHeader("Authorization", "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiI2ZjdjM2E5MS0xY2JjLTM3OWEtYjEyYS0yYjA0YzQ5ZjFmYzIiLCJjb250ZXh0Ijp7ImJhc2VVcmwiOiJodHRwczpcL1wvcHJhdmFsbGlrYXNlZ3UuYXRsYXNzaWFuLm5ldCIsInVzZXIiOnsiYWNjb3VudElkIjoiNWZiODFmZDJkNjcwYjgwMDZlOGJhYWE4In19LCJpc3MiOiJjb20ua2Fub2FoLnRlc3QtbWFuYWdlciIsImV4cCI6MTY0MDI4NzAzOCwiaWF0IjoxNjA4NzUxMDM4fQ.ami_8HoCuLiGufKjKVIsle_HEpX8hj2kE8J1Mrgad90")
#      request.SetHeader("Content-Type", "application/json")
#      request.SetHeader("projectKey","ZSDAP")
#      aqHttpResponse = request.Send()    
#      if aqHttpResponse != None:
#          # Read the response data
#          Log.Message(aqHttpResponse.AllHeaders) # All headers
#          Log.Message(aqHttpResponse.GetHeader("Content-Type")) # A specific header
#          Log.Message(aqHttpResponse.StatusCode) # A status code
#          Log.Message(aqHttpResponse.StatusText) # A status text
#          Log.Message(aqHttpResponse.Text) # A response body
#          # Save the response body to a file and place it to the project folder
#          aqHttpResponse.SaveToFile(Project.Path + "body.txt")

          
#def createTestCases():
#      address=baseURL+"/testcases"
#      request = aqHttp.CreatePostRequest(address)
##      Log.Message(Project.Variables.BearerToken)
##      request.SetHeader("Authorization", Project.Variables.BearerToken)
#      request.SetHeader("Authorization", "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiI2ZjdjM2E5MS0xY2JjLTM3OWEtYjEyYS0yYjA0YzQ5ZjFmYzIiLCJjb250ZXh0Ijp7ImJhc2VVcmwiOiJodHRwczpcL1wvcHJhdmFsbGlrYXNlZ3UuYXRsYXNzaWFuLm5ldCIsInVzZXIiOnsiYWNjb3VudElkIjoiNWZiODFmZDJkNjcwYjgwMDZlOGJhYWE4In19LCJpc3MiOiJjb20ua2Fub2FoLnRlc3QtbWFuYWdlciIsImV4cCI6MTY0MDI4NzAzOCwiaWF0IjoxNjA4NzUxMDM4fQ.ami_8HoCuLiGufKjKVIsle_HEpX8hj2kE8J1Mrgad90")
#      request.SetHeader("Content-Type", "application/json")
##      request.SetHeader("projectKey","ZSDAP")
#      requestBody = '{"projectKey": "ZSDAP","name": "Created from APIs","precondition": "login into app","estimatedTime": 138000,"priorityName": "Low"}'
#      aqHttpResponse = request.Send(requestBody)    
#      if aqHttpResponse != None:
#          # Read the response data
#          Log.Message(aqHttpResponse.AllHeaders) # All headers
##          Log.Message(aqHttpResponse.GetHeader("Content-Type")) # A specific header
#          Log.Message(aqHttpResponse.StatusCode) # A status code
#          Log.Message(aqHttpResponse.StatusText) # A status text
#          Log.Message(aqHttpResponse.Text) # A response body
#          # Save the response body to a file and place it to the project folder
#          aqHttpResponse.SaveToFile(Project.Path + "body.txt")     
          
def getTestCaseID(argument):
    #list out testcaseID's in dict format - this is where you will map your internal testcases (by name) to their corresponding tm4j testcases
    id_dict = {
        "Test1": "ZSDAP-T4",
        }
   
    tc_ID = id_dict.get(argument, "Invalid testCase") #get testcase keys by name from dictionary above
    Log.Message("Test case id is ")
    Log.Message(tc_ID)
    return tc_ID  #output tm4j testcase ID
    
    
          
def createTestRun():
    address=baseURL+"/testexecutions"
    request = aqHttp.CreatePostRequest(address)
#      Log.Message(Project.Variables.BearerToken)
#      request.SetHeader("Authorization", Project.Variables.BearerToken)
    request.SetHeader("Authorization", "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiI2ZjdjM2E5MS0xY2JjLTM3OWEtYjEyYS0yYjA0YzQ5ZjFmYzIiLCJjb250ZXh0Ijp7ImJhc2VVcmwiOiJodHRwczpcL1wvcHJhdmFsbGlrYXNlZ3UuYXRsYXNzaWFuLm5ldCIsInVzZXIiOnsiYWNjb3VudElkIjoiNWZiODFmZDJkNjcwYjgwMDZlOGJhYWE4In19LCJpc3MiOiJjb20ua2Fub2FoLnRlc3QtbWFuYWdlciIsImV4cCI6MTY0MDI4NzAzOCwiaWF0IjoxNjA4NzUxMDM4fQ.ami_8HoCuLiGufKjKVIsle_HEpX8hj2kE8J1Mrgad90")
    request.SetHeader("Content-Type", "application/json")
    Log.Message(Project.TestItems.ItemCount)
    items= []
#    for i in range(2): #for all test items listed at the project level
#        entry = {"testCaseKey":getTestCaseID(Project.TestItems.TestItem[i].Name)} #grab each tc key as value pair according to name found in id_dict
#        items.append(entry) #append as a test item
      
      #building request body
    Log.Message(items)
    requestBody = {
        "projectKey" : "ZSDAP", #jira project key for the tm4j project
        "testCycleKey":"ZSDAP-R2",
        "statusName": "Pass",
        "testCaseKey":"ZSDAP-T4"
#        "items" : items #the items list will hold the key value pairs of the test case keys to be added to this test cycle
      }
    response = request.Send(json.dumps(requestBody))
    if response != None:
              # Read the response data
              Log.Message(response.AllHeaders) # All headers
              Log.Message(response.GetHeader("Content-Type")) # A specific header
              Log.Message(response.StatusCode) # A status code
              Log.Message(response.StatusText) # A status text
              Log.Message(response.Text) # A response body
              Log.Message(json.dumps(requestBody))
              # Save the response body to a file and place it to the project folder
              response.SaveToFile(Project.Path + "body.txt")
#    df =  json.loads(response.Text)
#    key = str(df["key"])
#    #set the new test cycle key as a project level variable for later use
#    Project.Variables.testRunKey = key
#    Log.Message(key) #output new test cycle key 


def createCyclebasedonEventHandle():
    address=baseURL+"/testcycles/ZSDAP-R2"
    request = aqHttp.CreatePostRequest(address)
#      Log.Message(Project.Variables.BearerToken)
#      request.SetHeader("Authorization", Project.Variables.BearerToken)
    request.SetHeader("Authorization", "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiI2ZjdjM2E5MS0xY2JjLTM3OWEtYjEyYS0yYjA0YzQ5ZjFmYzIiLCJjb250ZXh0Ijp7ImJhc2VVcmwiOiJodHRwczpcL1wvcHJhdmFsbGlrYXNlZ3UuYXRsYXNzaWFuLm5ldCIsInVzZXIiOnsiYWNjb3VudElkIjoiNWZiODFmZDJkNjcwYjgwMDZlOGJhYWE4In19LCJpc3MiOiJjb20ua2Fub2FoLnRlc3QtbWFuYWdlciIsImV4cCI6MTY0MDI4NzAzOCwiaWF0IjoxNjA4NzUxMDM4fQ.ami_8HoCuLiGufKjKVIsle_HEpX8hj2kE8J1Mrgad90")
    request.SetHeader("Content-Type", "application/json")
    Log.Message(Project.TestItems.ItemCount)
    items= []
#    for i in range(2): #for all test items listed at the project level
#        entry = {"testCaseKey":getTestCaseID(Project.TestItems.TestItem[i].Name)} #grab each tc key as value pair according to name found in id_dict
#        items.append(entry) #append as a test item
      
      #building request body
    Log.Message(items)
    requestBody = {
        "projectKey" : "ZSDAP", #jira project key for the tm4j project
        "testCycleKey":"ZSDAP-R2",
        "statusName": "Pass",
        "testCaseKey":"ZSDAP-T4"
#        "items" : items #the items list will hold the key value pairs of the test case keys to be added to this test cycle
      }
    response = request.Send(json.dumps(requestBody))
    if response != None:
              # Read the response data
              Log.Message(response.AllHeaders) # All headers
              Log.Message(response.GetHeader("Content-Type")) # A specific header
              Log.Message(response.StatusCode) # A status code
              Log.Message(response.StatusText) # A status text
              Log.Message(response.Text) # A response body
              Log.Message(json.dumps(requestBody))
              # Save the response body to a file and place it to the project folder
              response.SaveToFile(Project.Path + "body.txt")
#    df =  json.loads(response.Text)
#    key = str(df["key"])
#    #set the new test cycle key as a project level variable for later use
#    Project.Variables.testRunKey = key
#    Log.Message(key) #output new test cycle key
