﻿import json
import base64

#init empty id dictionary
id_dict = {}


def httpGetRequest():
  address = "http://httpbin.org"
#testcase/{ZSDAP-T1}
  # Create an aqHttpRequest object
  aqHttpRequest = aqHttp.CreateGetRequest("https://pravallikasegu.atlassian.net/rest/api/3/search")

  # Send the request, get an aqHttpResponse object
  aqHttpResponse = aqHttpRequest.Send()

  if aqHttpResponse != None:
    # Read the response data
    Log.Message(aqHttpResponse.AllHeaders) # All headers
    Log.Message(aqHttpResponse.GetHeader("Content-Type")) # A specific header
    Log.Message(aqHttpResponse.StatusCode) # A status code
    Log.Message(aqHttpResponse.StatusText) # A status text
    Log.Message(aqHttpResponse.Text) # A response body
    
    # Save the response body to a file and place it to the project folder
    aqHttpResponse.SaveToFile(Project.Path + "body.txt")
#    
#def simplegetTCInfocall():
#  projName = "Automated_TestComplete_Run " + str(aqDateTime.Now()) #timestamped test cycle name 
#  address =  "https://pravallikasegu.atlassian.net/rest/atm/1.0/testrun" #TM4J endpoint to create test run
#  username = "psegu9@gmail.com" #TM4J username
#  password = "Common@123" #TM4J password
#  # Convert the user credentials to base64 for preemptive authentication
#  #credentials = base64.b64encode((username + ":" + password).encode("ascii")).decode("ascii")
#  credentials=username + ":" + password
#  
#  request = aqHttp.(address)
#  request.SetHeader("Authorization", "Basic " + credentials)
#  request.SetHeader("Content-Type", "application/json")
#  /testcase/{testCaseKey}

#def createTestRun():
#      projName = "Automated_TestComplete_Run " + str(aqDateTime.Now()) #timestamped test cycle name 
#      address =  "https://pravallikasegu.atlassian.net/rest/atm/1.0/testrun" #TM4J endpoint to create test run
#      username = "psegu9@gmail.com" #TM4J username
#      password = "Common@123" #TM4J password
#      # Convert the user credentials to base64 for preemptive authentication
#      #credentials = base64.b64encode((username + ":" + password).encode("ascii")).decode("ascii")
#      credentials=username + ":" + password
#  
#      request = aqHttp.CreatePostRequest(address)
#      request.SetHeader("Authorization", "Basic " + credentials)
#      request.SetHeader("Content-Type", "application/json")
#      
#      #intialize empty item list
#      items= []
#      for i in range(Project.TestItems.ItemCount): #for all test items listed at the project level
#        entry = {"testCaseKey":getTestCaseID(Project.TestItems.TestItem[i].Name)} #grab each tc key as value pair according to name found in id_dict
#        items.append(entry) #append as a test item
#      
#      #building request body
#      requestBody = {
#        "name" :"Zephyr Scale Dummy ADSS Project",
#        "projectKey" : "ZSDAP", #jira project key for the tm4j project
#        "items" : items #the items list will hold the key value pairs of the test case keys to be added to this test cycle
#      }
#      response = request.Send(json.dumps(requestBody))
#      df =  json.loads(response.Text)
#      key = str(df["key"])
#      #set the new test cycle key as a project level variable for later use
#      Project.Variables.testRunKey = key
#      Log.Message(key) #output new test cycle key