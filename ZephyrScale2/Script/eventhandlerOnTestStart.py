﻿import json
baseURL="https://api.tm4j.smartbear.com/rest-api/v2"
def EventControl1_OnStartTest(Sender):
#    testrunKey = Project.Variables.testRunKey #grab testrun key from createstRun script
#    tc_name = aqTestCase.CurrentTestCase.Name #grab current testcase name to provide to getTestCaseID function below
#    tcKey = utilities.getTestCaseID(tc_name) #return testcase Key for required resource path below
    address=baseURL+"/testcycles/703513"
    request = aqHttp.CreateRequest("PUT",address)
#      Log.Message(Project.Variables.BearerToken)
#      request.SetHeader("Authorization", Project.Variables.BearerToken)
    request.SetHeader("Authorization", "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiI2ZjdjM2E5MS0xY2JjLTM3OWEtYjEyYS0yYjA0YzQ5ZjFmYzIiLCJjb250ZXh0Ijp7ImJhc2VVcmwiOiJodHRwczpcL1wvcHJhdmFsbGlrYXNlZ3UuYXRsYXNzaWFuLm5ldCIsInVzZXIiOnsiYWNjb3VudElkIjoiNWZiODFmZDJkNjcwYjgwMDZlOGJhYWE4In19LCJpc3MiOiJjb20ua2Fub2FoLnRlc3QtbWFuYWdlciIsImV4cCI6MTY0MDI4NzAzOCwiaWF0IjoxNjA4NzUxMDM4fQ.ami_8HoCuLiGufKjKVIsle_HEpX8hj2kE8J1Mrgad90")
    request.SetHeader("Content-Type", "application/json")
    comment = "posting from TestComplete" #default comment to test executions
#    if StopTestCaseParams.Status == 0: # lsOk
#        statusId = "Pass" # Passed
#    elif StopTestCaseParams.Status == 1: # lsWarning
#        statusId = "Warning" # Passed with a warning
#        comment = StopTestCaseParams.FirstWarningMessage
#    elif StopTestCaseParams.Status == 2: # lsError
#        statusId = "Fail" # Failed
#        comment = StopTestCaseParams.FirstErrorMessage
#    
      
      #building request body
  
    requestBody = {
        "id":"703513",
        "key":"ZSDAP-R2",
        "name": "User Story 2",
        "project":{"id":59101,"self":"https://api.tm4j.smartbear.com/rest-api/v2/projects/59101"},
        "status":{"id":1034707}

      }
    response = request.Send(json.dumps(requestBody))
#    if response.StatusCode != 201:
#          Log.Warning("Failed to send results to TM4J. See the Details in the previous message.")
    if response != None:
              # Read the response data
              Log.Message(response.AllHeaders) # All headers
#              Log.Message(response.GetHeader("Content-Type")) # A specific header
              Log.Message(response.StatusCode) # A status code
              Log.Message(response.StatusText) # A status text
              Log.Message(response.Text) # A response body
              Log.Message(json.dumps(requestBody))
              # Save the response body to a file and place it to the project folder
              response.SaveToFile(Project.Path + "body.txt")